## Javascript backend library to verify keycloak tokens and get user info


### Instantiating
```js
   const config = { realm: 'OECD', authServerUrl: 'localhost://8080' };
   const keycloak = Keycloak(config);
```

### Validating access tokens


#### Online validation
This method requires online connection to the Keycloak service to validate the access token. It is highly secure since it also check for the possible token invalidation. The disadvantage is that a request to the Keycloak service happens on every validation attempt.
```js
try {
   const user = await keycloak.verifyOnline(accessToken);
} catch(e){ ... }
```

Will request `keycloak` server to get user information, throw an execption if token is not valid. 
Response data is a subset of `verify Offline` to add more attributes see https://www.keycloak.org/docs/latest/server_admin/index.html#user-attributes or https://stackoverflow.com/questions/32678883/keycloak-retrieve-custom-attributes-to-keycloakprincipal



#### Offline validation

This method can be used in two different ways:

* With a `public key` prop in the config object, the function verify the access token and returns user information.

```js
   const config = { publicKey: keycloakPublicKey };
```
Performance is higher compared to the online method, the disadvantage is that access token invalidation will not work until the token is expired.

* Without a `public key`, then config needs a `realm` and a `authServerUrl` set in the config to retrieve it from keycloak server.
```js
   const config = { realm: 'OECD', authServerUrl: 'localhost://8080' };
```

We can add `config.useCache` to cache keycloak public key to optimize performance and avoid to request it on each verification.

```js
try {
   const user = await keycloak.verifyOffline(accessToken);
} catch(e){ ... }
```
Decodes token to get user information, throw an exception if token is not valid.

