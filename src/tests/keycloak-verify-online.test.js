import Keycloak from "..";

const config = { realm: "realm", authServerUrl: "url" };
const keycloak = Keycloak(config);

jest.mock("axios", () => ({
  get: (url, options) =>
    Promise.resolve({ data: { email: url, name: options } })
}));

describe("Keycloak Verify", () => {
  describe("verify online", () => {
    it("should get UserInfo", async () => {
      const token = "TOKEN";
      const user = await keycloak.verifyOnline(token);
      expect(user.email).toMatch(config.authServerUrl);
      expect(user.name.headers.Authorization).toEqual(`Bearer ${token}`);
    });

    it("should get UserInfo with injected realm", async () => {
      const token = "TOKEN";
      const realm = "NEW_REALM";
      const user = await keycloak.verifyOnline(token, { realm });
      expect(user.email).toMatch(config.authServerUrl);
      expect(user.email).toMatch(realm);
      expect(user.name.headers.Authorization).toEqual(`Bearer ${token}`);
    });
  });
});
